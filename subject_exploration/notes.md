# Notes

In this section, we compile a collection of our notes and 'Aha! moments.' These are insights, observations, and key takeaways that we've gathered during our exploration and study of various topics. It serves as a reservoir of knowledge and valuable insights that we've encountered on our learning journey. Feel free to explore and expand your understanding with the information presented here.

- Arrays
- Linked Lists (Singly and Doubly)
- Stacks and Queues
- Hash Tables
- Trees
- Linear Search
- Binary Search
- Bubble Sort
- Selection Sort
- Insertion Sort
- Merge Sort
- Quick Sort
- FIFO
- FILO
- Constant runtime O(1)
- Logarithmic runtime O(log n)
- Linear runtime O(n)
- Quadratic runtime O(n^2)
- Quasilinear runtime O(n log n)
- Cubic runtime O(n^3)
- Factorial runtime O(n!)
- Exponential runtime O(x^n)
- Polynomial runtime O(n^x)
- Brute force technique
- Traveling salesman
- Recursion
- Interface
- Best, average, and worst-case scenarios
- We measure efficiency by its worst-case scenario
- Time complexity is how fast or slow a task is completed
- Space complexity is how many resources we need to complete a task
- The growth rate is when we plotted the inputs and see how the algorithm will perform
- Big O notation is the theoretical definition of the complexity of an algorithm as a function of size

### Installing Python

#### For Windows

1. Go to "Microsoft store"
2. Search for Python
3. Install Python

![alt install-python-windows.jpg](/images/install-python-windows.png)

#### For macOS

1. [Download this](https://www.python.org/ftp/python/3.10.7/python-3.10.7-macos11.pkg)
2. Follow installation guide
